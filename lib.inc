section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax	
  .loop:
    cmp byte[rdi + rax], 0 
    je .end 
    inc rax 
    jmp .loop
.end:
    ret 

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    call string_length 
    mov rsi, rdi 
    mov rdx, rax 
    mov rdi, 1 
    mov rax, 1 
    syscall 
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax 
    push rdi 
    mov rsi, rsp 
    mov rax, 1
    mov rdx, 1
    mov rdi, 1
    syscall
    pop rdi
    ret
	
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 10
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
  mov rax, rdi
  xor rcx, rcx
  mov rsi, 10
.loop:
  xor rdx, rdx
  div rsi
  add rdx, '0'
  dec rsp
  mov [rsp], dl
  inc rcx
  test rax, rax
  jnz .loop
  mov rax, 1
  mov rdi, 1
  mov rsi, rsp
  mov rdx, rcx
  push rcx
  syscall
  pop rcx
  add rsp, rcx
  ret
	
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    test rdi, rdi 
    jns print_uint 
    push rdi 
    mov rdi, '-' 
    call print_char 
    pop rdi 
    neg rdi 
    jmp print_uint 
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
  .loop:
    mov al, [rdi+rcx] ; 8bit -> al
    cmp [rsi+rcx], al ; сравниваем первый байт первой и второй строки
    jne .ret_n_equal ; если они не равни
    test al, al ; сравниваем 0
    jz .ret_equal
    inc rcx
    jmp .loop
  .ret_equal:
    mov rax, 1
    ret
  .ret_n_equal:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax 
    xor rdi, rdi 
    mov rdx, 1 
    push 0 
    mov rsi, rsp 
    syscall
    pop rax 
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rax, rax
    push r12
    push r13
    xor r12, r12
    xor r13, r13
    mov r13, rsi
	
  .skip_whitespace:
    push rdi ; сохраняем состояние rdi (read_char использует rdi)
    call read_char ; читаем символ
    pop rdi ; возвращаем rdi
    cmp al, 0x20 
    je .skip_whitespace
    cmp al, 0x9
    je .skip_whitespace       ; Проверяем на пробельные символы
    cmp al, 0xA
    je .skip_whitespace
    cmp al, 0x13
    je .skip_whitespace
    test al, al ; если конец строки переходим на end
    jz .end
  .loop:
    mov byte[rdi + r12], al ; Если прошел проверку читаем символ в память
    inc r12 ; счетчик 
    push rdi ; read_char использует rdi
    call read_char
    pop rdi
    cmp al, 0x20
    je .end
    cmp al, 0x9
    je .end       ; проверяем на пробельные символи если находим заканчиваем чтение символов
    cmp al, 0xA
    je .end
    cmp al, 0x13
    je .end
    test al, al
    jz .end
    cmp r13, r12 ; если символов много чем надо переходим в end
    je .err
    jmp .loop

  .end:
    mov byte[rdi + r12], 0 ; добавляем конец строки
    mov rax, rdi ; Возвращаем начало строки 
    mov rdx, r12  ; Длина строки
    pop r13
    pop r12 
    ret
	 
  .err:
    xor rax, rax
    pop r13
    pop r12
    ret

	
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    mov r8, 10 ; Значение для умножения
    xor rcx, rcx
    xor r9, r9
  .loop:
    mov r9b, byte[rdi + rcx] ; читаем первый символ
    cmp r9b, 0x30 ; сравниваем с 0
    jb .end
    cmp r9b, 0x39 ; сравниваем с 9
    ja .end
    mul r8 ; умнажаем rax  на 10
    sub r9b, 0x30 ; вычитаем 0 чтобы получить 10cc
    add rax, r9 ; добавляем в аккумулятор
    inc rcx ; счетчик
    jmp .loop
    ret 
  .end:
    mov rdx, rcx ; возвращаем длину
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte[rdi] 
    cmp al, '-' ; смотрим является ли число отриц.
    je .negative ; если знакковый negative
    jmp parse_uint ; если беззнаковый

  .negative: 
    inc rdi ; следующи символ за знаковым
    call parse_uint ; вызываем parce_uint
    neg rax  ; сделаем число отрицательным
    test rdx, rdx ; смотрим получилось ли прочитать число
    jz .err ;  
    inc rdx ; включаем знак
    ret

  .err:
    xor rax, rax
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
  xor rax, rax
  test rdx, rdx
  jz .exit
.loop:
  mov cl, [rdi+rax]
  mov [rsi+rax], cl
  test cl, cl
  jz .exit
  inc rax
  cmp rax, rdx
  jle .loop
  xor rax, rax
.exit:
  ret